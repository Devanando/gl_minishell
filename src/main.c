/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: devanando <devanando@student.codam.nl>       +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/10 19:24:52 by devanando      #+#    #+#                */
/*   Updated: 2019/10/10 19:24:52 by devanando     ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include "env.h"
#include "minishell.h"

int		main(int ac, char **av, char **envp)
{
	t_env *env;

	if (ac > 2 || (av[1]))
		ft_printf("No input needed here !?");
	env = env_new(envp);
	return (cetush(env));
}
