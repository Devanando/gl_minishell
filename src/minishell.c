/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: devanando <devanando@student.codam.nl>       +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/10 19:24:25 by devanando      #+#    #+#                */
/*   Updated: 2019/10/10 19:24:25 by devanando     ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <stdlib.h>
#include <ft_printf.h>
#include <sys/wait.h>
#include "minishell.h"
#include "env.h"

void		env_builtins(struct s_env *env, char **splitted);

void		free_str_arr(char ***str_arr)
{
	int i;

	i = 0;
	while ((*str_arr)[i] != NULL)
	{
		ft_strdel(&(*str_arr)[i]);
		i++;
	}
	ft_memdel((void **)str_arr);
}

int			execute(char **argv, t_env *env)
{
	char	**envp;
	pid_t	pid;
	int		stat_loc;

	envp = env_to_envp(env);
	if (envp == NULL)
	{
		ft_dprintf(2, "unable to convert env");
		return (-1);
	}
	pid = fork();
	if (pid == 0)
	{
		if (execve(argv[0], argv, envp) == -1)
		{
			ft_dprintf(2, "unable to launch process\n");
			return (-1);
		}
		return (42);
	}
	else
	{
		waitpid(pid, &stat_loc, 0);
		return (WEXITSTATUS(stat_loc));
	}
}

int			cetush(t_env *env)
{
	char *line;
	char **splitted;
	int ret;
	int status;

	status = 0;
	while (1)
	{
		ft_printf("cetush $ ");
		ret = ft_getline(0, &line);
		if (ret == 0)
			return (0);
		if (ret == -1)
		{
			ft_dprintf(2, "unable to read input\n");
			return (1);
		}
		ft_printf("get line: %s\n", line);
		ft_strreplace(&line, expansions(env, line));
		if (line == NULL)
		{
			ft_dprintf(2, "unable to expand environment variables\n");
			return (1);
		}
		ft_printf("expanded line: %s\n", line);
		//
		splitted = ft_strsplit(line, ' ');
		ft_strdel(&line);
		if (splitted == NULL)
		{
			ft_dprintf(2, "unable to parse input\n");
			return (1);
		}	ft_putendl("TEEEEEESSSSSTTT");
		//
		ft_printf("splitted line[0]: %s\n", splitted[0]);
		status = execute(splitted, env);
		env_builtins(env, splitted);
		ft_printf("status: %d\n", status);
		free_str_arr(&splitted);
		ft_putendl("TEEEEEESSSSSTTT");
	}
	return (1);
}
