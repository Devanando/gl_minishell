/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_cd.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: Invalid date        by                #+#    #+#                */
/*   Updated: Invalid date        by               ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */


#include <ft_printf.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <libft.h>
#include <unistd.h>
#include "env.h"
#include "minishell.h"

void	print_env(struct s_env *env, char **splitted)
{
	if (env == NULL)
		return ;
	if (splitted[1] && splitted[1][0] == '-' && splitted[1][1] == 'n' &&
			ft_strlen(splitted[1]) < 3)
		ft_printf("%s=%s", env->key, env->value);
	else
		ft_printf("%s=%s%c", env->key, env->value, '\n');
	return (print_env(env->next, splitted));
}

void		set_env(struct s_env **env, char **splitted)
{
	int		i;
	char	**key_value;

	i = 1;
	while (splitted[i] != NULL)
	{
		key_value = ft_strsplit(splitted[i], '=');
		env_set(env, key_value[0], key_value[1]);
		free_str_arr(&key_value);
		i++;
	}
}

void		unset_env(struct s_env **env, char **splitted)
{
	int		i;
	char	**key_value;

	i = 1;
	while (splitted[i] != NULL)
	{
		key_value = ft_strsplit(splitted[i], '=');
		env_unset(env, key_value[0]);
		free_str_arr(&key_value);
		i++;
	}
}

void		set_pwds(struct s_env *env, char *cwd)
{
	char	*old_pwd;

	old_pwd = env_get(env, "PWD");
	env_set(&env, "OLDPWD", old_pwd);
	env_set(&env, "PWD", cwd);
	ft_strdel(&old_pwd);
}

void		ft_cd(struct s_env *env, char **arg)
{
	char	*to_path;
	char	cwd[PATH_MAX];

	getcwd(cwd, PATH_MAX);
	if (arg[2] != NULL)
	{
		ft_printf("To many arguments");
		return ;
	}
	if (arg[1] == NULL)
		to_path = env_get(env, "HOME");
	else if (arg[1][0] == '~' && arg[1][1] == '\0')
		to_path = ft_strdup("$HOME");
	else if (arg[1][0] == '-' && arg[1][1] == '\0')
	{
		to_path = env_get(env, "OLDPWD");
		ft_printf("%s\n", to_path);
	}
	else if (arg[1] != NULL && arg[2] == NULL)
		to_path = ft_strdup(arg[1]);
	expansions(env, to_path);
	if (chdir(to_path) == 0)
		set_pwds(env, cwd);
	else
		ft_printf("Incorrect Path\n");
	ft_strdel(&to_path);
}

void		env_builtins(struct s_env *env, char **splitted)
{
	if (ft_strcmp("env", splitted[0]) == 0)
		print_env(env, splitted);
	if (ft_strcmp("setenv", splitted[0]) == 0)
		set_env(&env, splitted);
	if (ft_strcmp("unsetenv", splitted[0]) == 0)
		unset_env(&env, splitted);
	if (ft_strcmp("cd", splitted[0]) == 0)
		ft_cd(env, splitted);
	if (ft_strcmp("exit", splitted[0]) == 0)
		exit(0);
}
