/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_printf.h>
#include "env.h"

static t_env	*env_new_(char **envp, struct s_env *tail)
{
	char			**splitted;
	struct s_env	*head;

	if (*envp == NULL)
		return (tail);
	head = ft_memalloc(sizeof(*head));
	if (head == NULL)
		return (NULL);
	splitted = ft_strsplit(*envp, '=');
	if (splitted == NULL)
	{
		ft_memdel((void **)&head);
		return (NULL);
	}
	head->key = splitted[0];
	head->value = splitted[1];
	head->next = tail;
	ft_memdel((void **)&splitted);
	return (env_new_(envp + 1, head));
}

t_env		*env_new(char **envp)
{
	return (env_new_(envp, NULL));
}

char		*env_get(struct s_env *env, char *key)
{
	if (env == NULL)
		return (NULL);
	if (ft_strcmp(env->key, key) == 0)
		return (ft_strdup(env->value));
	return (env_get(env->next, key));
}

int			env_set(struct s_env **env, char *key, char *value)
{
	struct s_env *head;

	(void)env_unset(env, key);
	head = ft_memalloc(sizeof(*head));
	if (head == NULL)
		return (-1);
	head->key = ft_strdup(key);
	head->value = ft_strdup(value);
	head->next = *env;
	if (head->key == NULL || head->value == NULL)
	{
		ft_strdel(&head->key);
		ft_strdel(&head->value);
		ft_memdel((void **)&head);
		return (-1);
	}
	*env = head;
	return (0);
}

int			env_unset(struct s_env **head, char *key)
{
	struct s_env *next;

	if (*head == NULL)
		return (-1);
	if (ft_strcmp((*head)->key, key) != 0)
		return (env_unset(&(*head)->next, key));
	next = (*head)->next;
	ft_strdel(&(*head)->key);
	ft_strdel(&(*head)->value);
	ft_memdel((void **)head);
	*head = next;
	return (0);
}

static int	env_length_(struct s_env *env, int state)
{
	if (env == NULL)
		return (state);
	return (env_length_(env->next, state + 1));
}

int			env_length(struct s_env *env)
{
	return (env_length_(env, 0));
}

char		**env_to_envp(struct s_env *env)
{
	char	**envp;
	int		i;

	envp = ft_memalloc((env_length(env) + 1) * sizeof(*envp));
	if (envp == NULL)
		return (NULL);
	i = 0;
	while (env != NULL)
	{
		if (ft_asprintf(&envp[i], "%s=%s", env->key, env->value) == -1)
			return (NULL); // TODO, free?
		i++;
		env = env->next;
	}
	return (envp);
}

void		env_free(struct s_env **head)
{
	if ((*head)->next != NULL)
		env_free(&(*head)->next);
	ft_strdel(&(*head)->key);
	ft_strdel(&(*head)->value);
	ft_memdel((void **)head);
}
