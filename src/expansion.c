/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   expansion.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: devanando <devanando@student.codam.nl>       +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/10 19:24:52 by devanando      #+#    #+#                */
/*   Updated: 2019/10/10 19:24:52 by devanando     ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include <ft_printf.h>
#include <libft.h>
#include "env.h"
#include "minishell.h"

static int	is_key_char(char c)
{
	return (!ft_isspace(c) && (ft_isalnum(c) || c == '_'));
}

/*
** src = "foo $BAR baz"
** name = "BAR"
** value = "bar_contents"
** ret = (new string, src not modified) "foo bar_contents baz"
*/

static char	*replace(const char *src, const char *name, const char *value,
			size_t *offset)
{
	char	*dest;
	size_t	dollar_index;
	size_t	after_name_index;

	if (value == NULL)
		value = "";
	dollar_index = *offset;
	after_name_index = dollar_index + 1 + ft_strlen(name);
	ft_asprintf(&dest, "%.*s%s%s",
		dollar_index, src, value, src + after_name_index);
	*offset = dollar_index + ft_strlen(value);
	return (dest);
}

static char	*expansion(const char *line, t_env *env, size_t *offset)
{
	char	*res;
	char	*value;
	char	*name;
	int		name_len;

	name_len = 1;
	while (line[*offset] != '$' && line[*offset] != '\0')
		(*offset)++;
	if (line[*offset] == '\0')
		return (ft_strdup(line));
	while (line[*offset + name_len] && is_key_char(line[*offset + name_len]))
		name_len++;
	ft_asprintf(&name, "%.*s", name_len, line + *offset);
	if (name == NULL)
		return (NULL);
	value = env_get(env, name + 1);
	res = replace(line, name + 1, value, offset);
	ft_strdel(&name);
	ft_strdel(&value);
	return (res);
}

char	*expansions(t_env *env, const char *line)
{
	size_t	offset;
	char	*new_line;

	offset = 0;
	new_line = ft_strdup(line);
	while (1)
	{
		ft_strreplace(&new_line, expansion(new_line, env, &offset));
		if (new_line == NULL)
			return (NULL);
		if (offset >= ft_strlen(new_line))
			return (new_line);
	}
}
