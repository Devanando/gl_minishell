GREEN=\033[32;01m
BLUE=\033[36m
RED=\033[31m
RESET=\033[0m

define notice
	$(info $(shell printf "$(BLUE)NOTICE$(RESET)\t%s" $(1)))
endef
