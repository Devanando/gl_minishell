/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENV_H
# define ENV_H

typedef struct		s_env
{
	char			*key;
	char			*value;
	struct s_env	*next;
}					t_env;

t_env				*env_new(char **envp);
char				*env_get(struct s_env *env, char *key);
int					env_set(struct s_env **env, char *key, char *value);
int					env_unset(struct s_env **env, char *key);
char				**env_to_envp(struct s_env *env);
void				env_free(struct s_env **env);

#endif
