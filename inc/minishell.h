/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell.h                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: devanando <devanando@student.codam.nl>       +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/10 19:16:42 by devanando      #+#    #+#                */
/*   Updated: 2019/10/10 19:16:42 by devanando     ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "env.h"

int		cetush(t_env *env);
char	*expansions(t_env *env, const char *line);

#endif
